import 'package:audioplayer/models/audiofile_metadata.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';

import '../main.dart';

class CustomPlayer extends StatefulWidget {
  int duration;
  List<AudioFileMetaData> audiofiles;
  AudioPlayer audioPlayer;

  CustomPlayer({this.duration, this.audiofiles, this.audioPlayer});
  @override
  _CustomPlayerState createState() => _CustomPlayerState();
}

class _CustomPlayerState extends State<CustomPlayer> {
  AudioPlayerState audioPlayerState = AudioPlayerState.PAUSED;
  int currentPosition = 0;
  void seekToSec(int p) {
    Duration newPos = Duration(seconds: p);
    widget.audioPlayer.seek(newPos);
    widget.duration = widget.audiofiles[Player.valueNotifier.value].duration;
  }

  Widget slider() {
    return Container(
      width: 250,
      child: Slider.adaptive(
        activeColor: Colors.blue,
        value: (currentPosition / 1000).floorToDouble(),
        max: (widget.duration / 1000).floorToDouble() + 1,
        onChanged: (value) {
          setState(() {
            seekToSec(value.toInt());
            // print(value.floorToDouble());
          });
        },
      ),
    );
  }

  void play() {
    setState(() {
      widget.audioPlayer.play(widget.audiofiles[Player.valueNotifier.value].id);
    });
  }

  void pause() {
    setState(() {
      widget.audioPlayer.pause();
    });
  }

  void setNext() {
    Player.valueNotifier.value++;

    setState(() {
      if (Player.valueNotifier.value < widget.audiofiles.length) {
        widget.duration =
            widget.audiofiles[Player.valueNotifier.value].duration;
        widget.audioPlayer
            .play(widget.audiofiles[Player.valueNotifier.value].id);
      } else {
        Player.valueNotifier.value = 0;
        widget.duration =
            widget.audiofiles[Player.valueNotifier.value].duration;
        widget.audioPlayer
            .play(widget.audiofiles[Player.valueNotifier.value].id);
      }
    });
  }

  void setPast() {
    Player.valueNotifier.value--;
    setState(() {
      if (Player.valueNotifier.value >= 0) {
        widget.duration =
            widget.audiofiles[Player.valueNotifier.value].duration;
        widget.audioPlayer
            .play(widget.audiofiles[Player.valueNotifier.value].id);
      } else {
        Player.valueNotifier.value = widget.audiofiles.length - 1;
        widget.duration =
            widget.audiofiles[Player.valueNotifier.value].duration;
        widget.audioPlayer
            .play(widget.audiofiles[Player.valueNotifier.value].id);
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    widget.audioPlayer.release();
    widget.audioPlayer.dispose();
    // audioCache.clearCache();
  }

  @override
  void initState() {
    widget.audioPlayer.onPlayerCompletion.listen((event) {
      setState(() {
        setNext();
      });
    });
    widget.audioPlayer.onPlayerStateChanged.listen((s) {
      setState(() {
        audioPlayerState = s;
      });
    });
    widget.audioPlayer.onAudioPositionChanged.listen((t) {
      setState(() {
        currentPosition = t.inMilliseconds;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(15),
            topRight: Radius.circular(15),
          ),
          gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [Colors.deepOrange, Colors.orange, Colors.orangeAccent])),
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * 0.2,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(getTimeString(currentPosition)),
              slider(),
              Text(
                getTimeString(widget.duration),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              IconButton(
                  iconSize: 40,
                  color: Colors.white,
                  icon: Icon(Icons.skip_previous),
                  onPressed: () {
                    setPast();
                  }),
              IconButton(
                iconSize: 40,
                color: Colors.white,
                icon: Icon(
                  audioPlayerState == AudioPlayerState.PAUSED ||
                          audioPlayerState == AudioPlayerState.STOPPED
                      ? Icons.play_arrow
                      : Icons.pause,
                ),
                onPressed: () {
                  audioPlayerState == AudioPlayerState.PAUSED ||
                          audioPlayerState == AudioPlayerState.STOPPED
                      ? play()
                      : pause();
                },
              ),
              IconButton(
                  iconSize: 40,
                  color: Colors.white,
                  icon: Icon(Icons.skip_next),
                  onPressed: () {
                    setNext();
                  })
            ],
          ),
        ],
      ),
    );
  }
}

String getTimeString(int milliseconds) {
  if (milliseconds == null) milliseconds = 0;
  String minutes =
      '${(milliseconds / 60000).floor() < 10 ? 0 : ''}${(milliseconds / 60000).floor()}';
  String seconds =
      '${(milliseconds / 1000).floor() % 60 < 10 ? 0 : ''}${(milliseconds / 1000).floor() % 60}';
  return '$minutes:$seconds';
}

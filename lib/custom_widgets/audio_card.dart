import 'package:audioplayer/api/shared_preferences.dart';
import 'package:audioplayer/models/audiofile_metadata.dart';
import 'package:flutter/material.dart';

class AudioCard extends StatefulWidget {
  Map<String, List<AudioFileMetaData>> audiofiles = {};
  final AudioFileMetaData audioFileMetaData;
  Color color;
  String playlistname;
  Function onTap;
  AudioCard({
    this.audioFileMetaData,
    this.color,
    this.audiofiles,
    this.playlistname,
    this.onTap,
  });

  @override
  _AudioCardState createState() => _AudioCardState();
}

class _AudioCardState extends State<AudioCard> {
  Widget build(BuildContext context) {
    return ListTile(
        tileColor: widget.color,
        leading: Image.asset('assets/images/basicSongImage.png'),
        title: Text(widget.audioFileMetaData.name ?? ''),
        subtitle: Text(getTimeString(widget.audioFileMetaData.duration) ?? ''),
        trailing: widget.playlistname == 'All-Songs'
            ? DropdownButtonHideUnderline(
                child: DropdownButton(
                    iconSize: 0,
                    items: [
                      DropdownMenuItem(
                        child: Icon(
                          Icons.more_horiz_outlined,
                        ),
                      ),
                      DropdownMenuItem(
                        onTap: () => Scaffold.of(context).openDrawer(),
                        child: Text(
                          'Create',
                        ),
                      ),
                      ...widget.audiofiles.keys.map((e) {
                        if (!e.contains('All-Songs')) {
                          return DropdownMenuItem(
                            onTap: () {
                              if (!widget.audiofiles[e]
                                  .contains(widget.audioFileMetaData)) {
                                widget.audiofiles[e]
                                    .add(widget.audioFileMetaData);
                                SharedData.code(widget.audiofiles);
                              }
                            },
                            child: Text(
                              e,
                            ),
                          );
                        }
                        setState(() {
                          return null;
                        });
                      }),
                    ],
                    onChanged: (val) {}),
              )
            : IconButton(
                icon: Icon(Icons.delete_forever),
                onPressed: widget.onTap,
              ));
  }
}

String getTimeString(int milliseconds) {
  if (milliseconds == null) milliseconds = 0;
  String minutes =
      '${(milliseconds / 60000).floor() < 10 ? 0 : ''}${(milliseconds / 60000).floor()}';
  String seconds =
      '${(milliseconds / 1000).floor() % 60 < 10 ? 0 : ''}${(milliseconds / 1000).floor() % 60}';
  return '$minutes:$seconds';
}

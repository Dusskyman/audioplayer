class AudioFileMetaData {
  final String id;
  String name;
  final int duration;

  AudioFileMetaData({
    this.id,
    this.name,
    this.duration,
  });
  AudioFileMetaData.fromMap(Map map)
      : this.id = map['id'],
        this.name = map['name'],
        this.duration = map['duration'];

  Map toMap() {
    return {'id': this.id, 'name': this.name, 'duration': this.duration};
  }
}

import 'dart:developer';
import 'dart:io';

import 'package:audioplayer/api/shared_preferences.dart';
import 'package:audioplayer/main.dart';
import 'package:audioplayer/models/audiofile_metadata.dart';
import 'package:audiotagger/audiotagger.dart';
import 'package:audiotagger/models/tag.dart';
import 'package:downloads_path_provider_28/downloads_path_provider_28.dart';
import 'package:flutter/material.dart';
import 'package:mp3_info/mp3_info.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoadScreen extends StatefulWidget {
  List<Tag> tag = [];
  List<String> pathToSong = [];
  Map<String, List<AudioFileMetaData>> audiofiles = {};
  LoadScreen() {
    _getDir();
  }

  void _getDir() async {
    SharedData.sharedPreferences = await SharedPreferences.getInstance();
    await DownloadsPathProvider.downloadsDirectory.then((value) {
      List<FileSystemEntity> pathTo =
          value.listSync(recursive: true, followLinks: false);
      pathTo.map((e) {
        if (e.toString().contains('.mp3') || e.toString().contains('.mp4')) {
          pathToSong.add(e.path);
        }
      }).toList();
      log(pathToSong.toString());
      _getDurationOnFirstTime();
    });
  }

  void _getDurationOnFirstTime() async {
    final tagger = Audiotagger();
    pathToSong.map((e) async {
      tag.add(await tagger.readTags(path: e));
    }).toList();
  }

  void _getMetaData() {
    log(tag.first.artist.toString());
    audiofiles['All-Songs'] = pathToSong.map((e) {
      return AudioFileMetaData(
          duration: MP3Processor.fromFile(File(e)).duration.inMilliseconds,
          id: e);
    }).toList();
    for (var i = 0;
        i < audiofiles[audiofiles.keys.toList().first].length;
        i++) {
      audiofiles[audiofiles.keys.toList().first][i].name = tag[i].title;
    }
  }

  @override
  _LoadScreenState createState() => _LoadScreenState();
}

class _LoadScreenState extends State<LoadScreen> with TickerProviderStateMixin {
  AnimationController _animationController;
  AnimationController _animationiconController;

  void goNextPage() async {
    await Permission.storage.request().then((value) {
      if (value.isGranted) {}
    });
  }

  @override
  void initState() {
    super.initState();
    _animationiconController = AnimationController(
      vsync: this,
      duration: Duration(seconds: 1),
    );

    _animationController = AnimationController(
      vsync: this,
      duration: Duration(seconds: 1),
    );
    _animationController.addListener(() {
      if (_animationController.isCompleted) {
        _animationiconController.forward();
      }
    });
    _animationiconController.addListener(
      () {
        if (_animationiconController.isCompleted) {
          widget._getMetaData();
          SharedData.code(widget.audiofiles);
          SharedData.decode(widget.audiofiles);
          // SharedData.check(widget.audiofiles);

          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (context) =>
                  Player(audiofilesPlayList: widget.audiofiles),
            ),
          );
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueAccent,
      body: AnimatedBuilder(
        animation: _animationController,
        builder: (context, child) {
          Future.delayed(Duration(seconds: 1), () {
            _animationController.forward();
          });
          return Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    bottomRight:
                        Radius.circular(1000 * _animationController.value)),
                gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: [
                      Colors.deepOrange,
                      Colors.orange,
                      Colors.orangeAccent,
                    ])),
            width:
                MediaQuery.of(context).size.width * _animationController.value,
            height:
                MediaQuery.of(context).size.height * _animationController.value,
            child: AnimatedBuilder(
              animation: _animationiconController,
              builder: (context, child) {
                return Opacity(
                  opacity: _animationiconController.value,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.queue_music_outlined,
                        size: 50,
                        color: Colors.white,
                      ),
                      Text(
                        'Beach Music',
                        style: TextStyle(fontSize: 20, color: Colors.white),
                      )
                    ],
                  ),
                );
              },
            ),
          );
        },
      ),
    );
  }
}

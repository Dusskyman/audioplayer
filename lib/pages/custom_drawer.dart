import 'package:audioplayer/api/shared_preferences.dart';
import 'package:audioplayer/main.dart';
import 'package:audioplayer/models/audiofile_metadata.dart';
import 'package:flutter/material.dart';

class CustomDrawer extends StatefulWidget {
  Map<String, List<AudioFileMetaData>> audiofilesPlaylists;
  Function setPlaylist;
  Function stop;
  CustomDrawer({this.setPlaylist, this.audiofilesPlaylists, this.stop});

  @override
  _CustomDrawerState createState() => _CustomDrawerState();
}

class _CustomDrawerState extends State<CustomDrawer> {
  TextEditingController _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Drawer(
        child: Scaffold(
          body: Builder(
            builder: (context) => SingleChildScrollView(
              physics: NeverScrollableScrollPhysics(),
              child: Column(
                children: [
                  ColoredBox(
                    color: Colors.blueGrey,
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 25),
                      child: TextField(
                          controller: _controller,
                          decoration: InputDecoration(
                              hintText: 'Введите название плэйлиста'),
                          onSubmitted: (value) {
                            widget.audiofilesPlaylists.addAll({'$value': []});
                            SharedData.code(widget.audiofilesPlaylists);
                            _controller.clear();
                          }),
                    ),
                  ),
                  ConstrainedBox(
                    constraints: BoxConstraints(
                        maxHeight: MediaQuery.of(context).size.height * 0.85),
                    child: ListView.builder(
                      itemCount: widget.audiofilesPlaylists.length,
                      itemBuilder: (context, index) => InkWell(
                        onTap: () {
                          if (widget
                                  .audiofilesPlaylists[widget
                                      .audiofilesPlaylists.keys
                                      .toList()[index]]
                                  .length !=
                              0) {
                            widget.stop();
                            Player.valueNotifier.value = 0;
                            widget.setPlaylist(widget.audiofilesPlaylists.keys
                                .toList()[index]);
                          } else
                            Scaffold.of(context).showSnackBar(SnackBar(
                                content: Text('Сначала добавьте трэк!')));
                        },
                        child: ListTile(
                          tileColor:
                              index % 2 == 0 ? Colors.orange : Colors.blue,
                          leading: Icon(Icons.playlist_add_check_sharp),
                          trailing: index != 0
                              ? IconButton(
                                  icon: Icon(Icons.delete_forever),
                                  onPressed: () {
                                    setState(() {
                                      widget.audiofilesPlaylists.remove(widget
                                          .audiofilesPlaylists.keys
                                          .toList()[index]);
                                      SharedData.delete(
                                          widget.audiofilesPlaylists);
                                    });
                                  })
                              : Icon(Icons.play_circle_fill),
                          title: Text(
                            widget.audiofilesPlaylists.keys.toList()[index],
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

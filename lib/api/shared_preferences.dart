import 'dart:convert';
import 'dart:developer';

import 'package:audioplayer/models/audiofile_metadata.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedData {
  static SharedPreferences sharedPreferences;

  static code(Map<String, List<AudioFileMetaData>> map) {
    map.forEach((key, value) {
      sharedPreferences.setStringList(
          key, value.map((e) => jsonEncode(e.toMap())).toList());
    });
    // sharedPreferences.getKeys().map((e) => log(e)).toList();
  }

  static delete(Map<String, List<AudioFileMetaData>> map) {
    sharedPreferences.getKeys().map((e) {
      if (!map.containsKey(e)) {
        sharedPreferences.remove(e);
      }
    }).toList();
  }

  static decode(Map<String, List<AudioFileMetaData>> map) {
    sharedPreferences.getKeys().map((e) {
      map.addAll({e: []});
    }).toList();
    // map.keys.map((e) => print('$e + key')).toList();
    map.keys.map((e) {
      map[e] = sharedPreferences
          .getStringList(e)
          .map((e) => AudioFileMetaData.fromMap(jsonDecode(e)))
          .toList();
    }).toList();
  }

  static check(Map<String, List<AudioFileMetaData>> map) {
    map.values.map((lists) {
      lists.map((list) {
        if (!map.values.first.contains(list)) {
          lists.remove(list);
        }
      }).toList();
    }).toList();
  }
}

import 'dart:async';

import 'package:audioplayer/api/shared_preferences.dart';
import 'package:audioplayer/custom_widgets/audio_card.dart';
import 'package:audioplayer/custom_widgets/custom_player.dart';
import 'package:audioplayer/pages/custom_drawer.dart';
import 'package:audioplayer/pages/load_screen.dart';
import 'package:audioplayer/models/audiofile_metadata.dart';
import 'package:flutter/material.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:permission_handler/permission_handler.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  void goNextPage(BuildContext context) async {
    await Permission.storage.request().then((value) {
      if (value.isGranted) {
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => LoadScreen(),
          ),
        );
      } else {
        SystemNavigator.pop();
      }
    });
  }

  static AudioPlayer mainAudioPlayer = AudioPlayer();
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Builder(
          builder: (context) {
            goNextPage(context);
            return Container();
          },
        ),
      ),
    );
  }
}

class Player extends StatefulWidget {
  // List<AudioFileMetaData> audiofiles;
  Map<String, List<AudioFileMetaData>> audiofilesPlayList;
  Player({this.audiofilesPlayList});
  static ValueNotifier<int> valueNotifier;
  @override
  _PlayerState createState() => _PlayerState();
}

class _PlayerState extends State<Player> {
  AudioPlayer audioPlayer = MyApp.mainAudioPlayer;
  int duration = 0;
  bool openDrawer = false;
  String appbarTitle = 'All-Songs';

  void setPlaylist(String key) {
    setState(() {
      appbarTitle = key;
      // widget.audiofiles = widget.audiofilesPlayList[key];
    });
  }

  void play() {
    setState(() {
      audioPlayer.play(widget
          .audiofilesPlayList[appbarTitle][Player.valueNotifier.value].id);
    });
  }

  void stop() {
    setState(() {
      audioPlayer.stop();
    });
  }

  @override
  void initState() {
    super.initState();
    Player.valueNotifier = ValueNotifier<int>(0);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      endDrawerEnableOpenDragGesture: false,
      drawer: CustomDrawer(
        stop: stop,
        setPlaylist: setPlaylist,
        audiofilesPlaylists: widget.audiofilesPlayList,
      ),
      appBar: AppBar(
        leading: Builder(
          builder: (context) => IconButton(
            icon: Icon(Icons.playlist_play),
            onPressed: () => Scaffold.of(context).openDrawer(),
          ),
        ),
        title: Text(appbarTitle),
      ),
      body: SizedBox(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: SingleChildScrollView(
          physics: NeverScrollableScrollPhysics(),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              SizedBox(
                width: double.infinity,
                height: MediaQuery.of(context).size.height * 0.7,
                child: ValueListenableBuilder(
                  valueListenable: Player.valueNotifier,
                  builder: (context, int value, child) {
                    return ListView.builder(
                        itemCount:
                            widget.audiofilesPlayList[appbarTitle].length,
                        itemBuilder: (context, index) {
                          return InkWell(
                            onTap: () {
                              setState(() {
                                Player.valueNotifier.value = index;
                                duration = widget
                                    .audiofilesPlayList[appbarTitle][index]
                                    .duration;
                                play();
                              });
                            },
                            child: AudioCard(
                                onTap: () {
                                  setState(() {});
                                  widget.audiofilesPlayList[appbarTitle]
                                      .removeAt(index);
                                  SharedData.code(widget.audiofilesPlayList);
                                  if (Player.valueNotifier.value == index &&
                                      Player.valueNotifier.value != 0) {
                                    Player.valueNotifier.value = 0;
                                  } else if (Player.valueNotifier.value == 0) {
                                    appbarTitle = 'All-Songs';
                                  }
                                },
                                playlistname: appbarTitle,
                                audiofiles: widget.audiofilesPlayList,
                                audioFileMetaData: widget
                                    .audiofilesPlayList[appbarTitle][index],
                                color: index == Player.valueNotifier.value
                                    ? Colors.blue
                                    : Colors.transparent),
                          );
                        });
                  },
                ),
              ),
              CustomPlayer(
                duration: widget
                    .audiofilesPlayList[appbarTitle][Player.valueNotifier.value]
                    .duration,
                audiofiles: widget.audiofilesPlayList[appbarTitle],
                audioPlayer: audioPlayer,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

String getTimeString(int milliseconds) {
  if (milliseconds == null) milliseconds = 0;
  String minutes =
      '${(milliseconds / 60000).floor() < 10 ? 0 : ''}${(milliseconds / 60000).floor()}';
  String seconds =
      '${(milliseconds / 1000).floor() % 60 < 10 ? 0 : ''}${(milliseconds / 1000).floor() % 60}';
  return '$minutes:$seconds';
}
